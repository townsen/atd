# After the Deadline container
#
FROM ubuntu:trusty
MAINTAINER nick.townsend@mac.com

RUN locale-gen en_US.UTF-8 && update-locale en_US.UTF-8
ENV LANG=en_US.utf8

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
    openjdk-7-jre-headless \
    && apt-get clean

EXPOSE 1049

RUN   mkdir -p /srv/atd
COPY  src/ /srv/atd/

WORKDIR /srv/atd
ENV   ATD_HOME=.
# This should be sent out of the container... TBD
ENV   LOG_DIR=$ATD_HOME/logs

# This is the low memory version: run-lowmem.sh

CMD   java -server \
      -XX:+AggressiveHeap -XX:+UseParallelGC \
      -Dfile.encoding=UTF-8 \
      -Datd.lowmem=true \
      -Dbind.interface=0.0.0.0 -Dserver.port=1049 \
      -Dsleep.classpath=$ATD_HOME/lib:$ATD_HOME/service/code \
      -Dsleep.debug=24 \
      -classpath ./lib/sleep.jar:./lib/moconti.jar:./lib/spellutils.jar:./lib/* \
      httpd.Moconti atdconfig.sl

# vim: set ft=dockerfile:
