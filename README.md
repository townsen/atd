# After The Deadline

This is an open source Grammar Checker that I've encapsulated into a Docker container for
use with the [Grass](https://github.com/townsen/grass) service.

See [the website](https://open.afterthedeadline.com/) for details

The code in the `src` directory is taken from the [current tarball](http://www.polishmywriting.com/download/atd_distribution081310.tgz)

## Building the container

Run `docker build -t atd:test .`

Nick Townsend
December 2016
